Pokedex.Router = Backbone.Router.extend({
  routes: {
    'pokemon': 'pokemonIndex',
    'pokemon/:id': "pokemonDetail",
    'pokemon/:pokemonId/toys/:toyId': "toyDetail"
  },

  pokemonDetail: function (id, callback) {
    if (typeof this._pokemonIndex === "undefined") {
      this.pokemonIndex(this.pokemonDetail.bind(this, id, callback));
      return;
    }

    var pokemon = this._pokemonIndex.pokes.get(id);
    this._pokemonDetail = new Pokedex.Views.PokemonDetail({ model: pokemon });
    $("#pokedex .pokemon-detail").html(this._pokemonDetail.$el);
    this._pokemonDetail.refreshPokemon();
  },

  pokemonIndex: function (callback) {
      this._pokemonIndex = new Pokedex.Views.PokemonIndex();
      this._pokemonIndex.refreshPokemon({success: callback});

      $("#pokedex .pokemon-list").html(this._pokemonIndex.$el);

  },

  toyDetail: function (pokemonId, toyId) {
    if (typeof this._pokemonDetail === "undefined") {
      this.pokemonDetail(pokemonId, this.toyDetail.bind(this, pokemonId, toyId));
      return;
    }

    var toy = this._pokemonDetail.model.toys().get(toyId);


    this._toyDetail = new Pokedex.Views.ToyDetail();
    $("#pokedex .toy-detail").html(this._toyDetail.$el);
    this._toyDetail.render({toy: toy});
  },

  pokemonForm: function () {
  }
});


$(function () {
  new Pokedex.Router();
  Backbone.history.start();
});
