Pokedex.Views = {}

Pokedex.Views.PokemonIndex = Backbone.View.extend({
  events: {
    "click li": "selectPokemonFromList"
  },

  initialize: function () {

    this.pokes = new Pokedex.Collections.Pokemon();

    this.listenToOnce(this.pokes, "sync", function() {
      this.listenTo(this.pokes, "add", this.render);
      this.render();
    });
  },

  addPokemonToList: function (pokemon) {
    var content = JST["pokemonListItem"]({pokemon: pokemon});
    this.$el.append(content);
  },

  refreshPokemon: function (options) {
    this.pokes.fetch(options);
  },

  render: function () {
    this.$el.empty();
    this.pokes.forEach(this.addPokemonToList.bind(this));
  },

  selectPokemonFromList: function (event) {
    var $target = $(event.currentTarget);

    var pokeId = $target.data('id');
    var pokemon = this.pokes.get(pokeId);

    Backbone.history.navigate("pokemon/" + pokeId, { trigger: true} );
  }
});

Pokedex.Views.PokemonDetail = Backbone.View.extend({
  events: {
    "click .toys li" : "selectToyFromList"
  },
  initialize: function () {
    this.listenTo(this.model, "sync", this.render);
  },

  refreshPokemon: function (options) {
    this.model.fetch();
  },

  render: function () {
    var content = JST["pokemonDetail"]({pokemon: this.model});

    this.$el.html(content);
    this.$el.find(".toys").empty();

    this.model.toys().each(function(toy) {
      var toyContent = JST["toyListItem"]({toy: toy});
      this.$el.find(".toys").append(toyContent);
    }.bind(this));
  },

  selectToyFromList: function (event) {
    var $target = $(event.currentTarget);
    var toyId = $target.data('id');

    Backbone.history.navigate("pokemon/" + $target.data("pokemon-id") + "/toys/" + toyId, { trigger: true} );
  }
});

Pokedex.Views.ToyDetail = Backbone.View.extend({
  render: function (toy) {
    var $detail = JST["toyDetail"]({toy: toy, pokes: this.pokes});

    this.$el.html($detail);
  }
});


$(function () {
  var pokemonIndex = new Pokedex.Views.PokemonIndex();
  pokemonIndex.refreshPokemon();
  $("#pokedex .pokemon-list").html(pokemonIndex.$el);
});
